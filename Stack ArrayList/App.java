import java.util.List;

public class App {
  public static void main(String [] args) {
    Stack stack = new Stack();
    stack.push("Satu");
    stack.push("Dua");
    stack.push("Tiga");
    stack.push("Empat");

    int count=stack.count();
    Object object = stack.peek();
    System.out.println("Jumlah Data Stack : " +count);
    System.out.println("Data Teratas Stack : "+object);

    System.out.println("--------------------------------");
    object=stack.pop();
    System.out.println("Objek yang dikeluarkan (POP) : "+object);
    count=stack.count();
    System.out.println("Jumlah Data Setelah POP : "+count);
    object=stack.peek();
    System.out.println("Data Teratas Setelah POP : "+object);
  }
}
